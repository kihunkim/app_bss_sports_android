package com.bss.sports.di

import com.bss.sports.BuildConfig
import com.bss.sports.network.AuthInterceptor
import com.bss.sports.network.ProtoApi
import com.bss.sports.network.SportsApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val TimeOut = if (BuildConfig.DEBUG) {
    20L
} else {
    5L
}

val networkModule = module {
    factory<Gson> { GsonBuilder().create() }

    factory {
        OkHttpClient.Builder().apply {
            connectTimeout(TimeOut, TimeUnit.SECONDS)
            readTimeout(TimeOut, TimeUnit.SECONDS)
            addInterceptor(AuthInterceptor())
            addInterceptor(HttpLoggingInterceptor().apply {
                if (BuildConfig.DEBUG) {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            })
        }.build()
    }

    factory<Retrofit> { (baseUrl: String) ->
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()
    }

    single<SportsApi> { get<Retrofit> { parametersOf(SportsApi.BaseUrl) }.create(SportsApi::class.java) }

    single<ProtoApi> { get<Retrofit> { parametersOf(ProtoApi.BaseUrl) }.create(ProtoApi::class.java) }
}