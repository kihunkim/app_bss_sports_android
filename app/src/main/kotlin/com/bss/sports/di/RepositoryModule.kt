package com.bss.sports.di

import com.bss.sports.model.sports.Soccer
import com.bss.sports.repository.proto.ProtoRepository
import com.bss.sports.repository.sports.*
import com.bss.sports.view.ui.calendar.CalendarRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory { PopularRepository(get()) }

    factory { SoccerRepository(get()) }

    factory { BaseballRepository(get()) }

    factory { BasketballRepository(get()) }

    factory { VolleyballRepository(get()) }

    factory { HockeyRepository(get()) }

    factory { FootballRepository(get()) }

    single { ProtoRepository(get()) }

    factory { CalendarRepository() }
}