package com.bss.sports.di

import android.content.Context
import android.content.SharedPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val prefModule = module {
    single { DefaultPreferences(androidContext()) }
}

class DefaultPreferences(context: Context) {
    private val preferences: SharedPreferences = context.getSharedPreferences("sports", Context.MODE_PRIVATE)
    private val guideOpenKey = "guide_open"

    fun setGuideOpen(guideOpen: Boolean) {
        preferences.edit().putBoolean(guideOpenKey, guideOpen).apply()
    }

    fun isGuideOpen() {
        preferences.getBoolean(guideOpenKey, false)
    }
}