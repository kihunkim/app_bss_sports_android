package com.bss.sports.di

import com.bss.sports.model.sports.Soccer
import com.bss.sports.repository.sports.*
import com.bss.sports.view.ui.calendar.CalendarViewModel
import com.bss.sports.view.ui.calendar.RoundSelectorViewModel
import com.bss.sports.view.ui.main.MainFragmentStateAdapter
import com.bss.sports.view.ui.main.proto.ProtoViewModel
import com.bss.sports.view.ui.main.sports.SportsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { (mainTab: MainFragmentStateAdapter.MainTab) ->
        SportsViewModel(
            when (mainTab) {
                MainFragmentStateAdapter.MainTab.주요경기 -> get<PopularRepository>()
                MainFragmentStateAdapter.MainTab.축구 -> get<SoccerRepository>()
                MainFragmentStateAdapter.MainTab.야구 -> get<BaseballRepository>()
                MainFragmentStateAdapter.MainTab.농구 -> get<BasketballRepository>()
                MainFragmentStateAdapter.MainTab.배구 -> get<VolleyballRepository>()
                MainFragmentStateAdapter.MainTab.아이스하키 -> get<HockeyRepository>()
                else -> get<FootballRepository>()
//                MainFragmentStateAdapter.MainTab.미식축구 -> get<FootballRepository>()
//                MainFragmentStateAdapter.MainTab.LOL -> TODO()
//                MainFragmentStateAdapter.MainTab.스타크래프트 -> TODO()

            }
        )
    }

    viewModel { ProtoViewModel(get()) }

    single { CalendarViewModel(get()) }

    single { RoundSelectorViewModel(get()) }
}