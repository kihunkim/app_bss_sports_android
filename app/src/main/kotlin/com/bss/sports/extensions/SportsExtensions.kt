package com.bss.sports.extensions

import com.bss.sports.model.sports.PeriodData

fun List<PeriodData>?.getTeamScore(): Int? = this?.sumBy { it.score }