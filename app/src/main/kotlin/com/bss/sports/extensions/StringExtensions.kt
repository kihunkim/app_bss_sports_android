package com.bss.sports.extensions

import java.util.Calendar

enum class Week {
    일, 월, 화, 수, 목, 금, 토
}

fun Calendar.getDayOfTheWeek(): String {
    val weekNumber: Int = Calendar.getInstance().apply { time = this@getDayOfTheWeek.time }.get(Calendar.DAY_OF_WEEK) - 1
    return Week.values()[weekNumber].name
}