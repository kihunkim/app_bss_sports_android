package com.bss.sports.network

import com.bss.sports.model.proto.ProtoListResponse
import com.bss.sports.model.proto.ProtoRound
import com.bss.sports.model.proto.RoundGame
import retrofit2.http.GET
import retrofit2.http.Query

interface ProtoApi {
    @GET("v1.0/schedule/proto/win")
    suspend fun getProtoWinList(@Query("round-id") roundId: Long? = null): ProtoListResponse

    @GET("v1.0/schedule/odds-history")
    suspend fun getProtoOddHistory(@Query("round-game-id") roundGameId: Long): RoundGame

    @GET("v1.0/schedule/round-history")
    suspend fun getProtoRoundHistory(@Query("roundType") roundType: String = "PROTO_WIN", @Query("year") year : String): List<ProtoRound>

    companion object {
        val BaseUrl = "https://api.picksmatch.com"
    }
}