package com.bss.sports.network

import com.bss.sports.model.sports.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.ArrayList

interface SportsApi {
    @GET("/v1.0/popular-games")
    suspend fun getPopularGames(@Query("date") date: String): LiveListResponse

    @GET("/v1.0/sports/soccer/games")
    suspend fun getSoccerList(@Query("date") date: String, @Query("status") status: String = "ALL"): List<Soccer>

    @GET("/v1.0/sports/baseball/games")
    suspend fun getBaseballList(@Query("date") date: String, @Query("status") status: String = "ALL"): List<Baseball>

    @GET("/v1.0/sports/basketball/games")
    suspend fun getBasketballList(@Query("date") date: String, @Query("status") status: String = "ALL"): List<Basketball>

    @GET("/v1.0/sports/volleyball/games")
    suspend fun getVolleyballList(@Query("date") date: String, @Query("status") status: String = "ALL"): List<Volleyball>

    @GET("/v1.0/sports/hockey/games")
    suspend fun getIceHockeyList(@Query("date") date: String, @Query("status") status: String = "ALL"): List<Hockey>

    @GET("/v1.0/sports/football/games")
    suspend fun getFootballList(@Query("date") date: String, @Query("status") status: String = "ALL"): List<Football>

    companion object {
        val BaseUrl = "https://api.picksmatch.com"
    }
}