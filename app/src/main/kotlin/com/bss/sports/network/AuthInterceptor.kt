package com.bss.sports.network

import com.bss.sports.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

const val ApiName = "named_sports_app"

val ApiKey = if (BuildConfig.isRelease) {
    "cIqA4H10EGO0"
} else {
    "o2SyGnweGUpQ"
}

class AuthInterceptor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request().newBuilder().apply {
            addHeader("oki-api-name", ApiName)
            addHeader("oki-api-key", ApiKey)
        }.build())
    }
}