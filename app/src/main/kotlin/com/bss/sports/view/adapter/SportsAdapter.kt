package com.bss.sports.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bss.sports.R
import com.bss.sports.model.sports.BaseSportsModel
import com.bss.sports.view.viewholder.SportsViewHolder

class SportsAdapter : RecyclerView.Adapter<SportsViewHolder>() {
    var items : List<BaseSportsModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SportsViewHolder {
        return SportsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sports, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SportsViewHolder, position: Int) {
        holder.bindItem(items[position])
    }
}