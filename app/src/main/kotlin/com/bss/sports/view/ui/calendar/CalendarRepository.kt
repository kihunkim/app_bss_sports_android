package com.bss.sports.view.ui.calendar

import androidx.lifecycle.MutableLiveData
import java.util.Calendar
import java.util.Date

class CalendarRepository {
    fun loadNowDateTime(): MutableLiveData<Calendar> {
        return MutableLiveData<Calendar>(Calendar.getInstance())
    }
}