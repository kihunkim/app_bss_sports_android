package com.bss.sports.view.ui.calendar

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.bss.sports.extensions.getDayOfTheWeek
import java.text.SimpleDateFormat
import java.util.Calendar

class CalendarViewModel(calendarRepository: CalendarRepository) : ViewModel() {
    @SuppressLint("SimpleDateFormat")
    private val DefaultSimpleFormatter = SimpleDateFormat("yyyy-MM-dd")
    private var _currentDate: MutableLiveData<Calendar> = calendarRepository.loadNowDateTime()
    val currentDate: LiveData<String> = Transformations.map(_currentDate) { calendar ->
        getMainDate(calendar, DefaultSimpleFormatter)
    }
    val currentDayOfWeek: LiveData<String> = Transformations.map(_currentDate) { calendar ->
        calendar.getDayOfTheWeek()
    }
    var isVisible: MutableLiveData<Boolean> = MutableLiveData(true)

    @SuppressLint("SimpleDateFormat")
    private fun getMainDate(calendar: Calendar, simpleFormatter: SimpleDateFormat): String {
        return simpleFormatter.format(calendar.time)
    }

    fun moveToNextDay() {
        this._currentDate.value = _currentDate.value?.apply { add(Calendar.DATE, 1) }
    }

    fun moveToPreviousDate() {
        this._currentDate.value = _currentDate.value?.apply { add(Calendar.DATE, -1) }
    }
}