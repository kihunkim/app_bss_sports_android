/*
 * Designed and developed by 2020 skydoves (Jaewoong Eum)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bss.sports.view.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bss.sports.view.ui.main.proto.ProtoFragment
import com.bss.sports.view.ui.main.sports.SportsFragment

class MainFragmentStateAdapter(fm: FragmentActivity) : FragmentStateAdapter(fm) {
    enum class MainTab {
        주요경기, 축구, 야구, 농구, 배구, 아이스하키, 미식축구, LOL, 스타크래프트, 프로토
    }

    override fun getItemCount(): Int {
        return MainTab.values().size
    }

    override fun createFragment(position: Int): Fragment {
        return if (MainTab.values()[position] == MainTab.프로토) {
            ProtoFragment.createFragment()
        } else {
            SportsFragment.createFragment(MainTab.values()[position])
        }
    }
}