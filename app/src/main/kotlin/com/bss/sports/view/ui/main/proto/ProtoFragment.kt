package com.bss.sports.view.ui.main.proto

import android.os.Bundle
import android.view.View
import com.bss.sports.R
import com.bss.sports.base.BindingFragment
import com.bss.sports.databinding.FragmentProtoBinding
import com.bss.sports.view.adapter.SportsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProtoFragment : BindingFragment<FragmentProtoBinding>() {
    companion object {
        fun createFragment(): ProtoFragment {
            return ProtoFragment()
        }
    }

    private val protoViewModel: ProtoViewModel by viewModel()

    override fun getLayoutResId(): Int = R.layout.fragment_proto

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            vm = protoViewModel
            lifecycleOwner = this@ProtoFragment
            sportsAdapter = SportsAdapter()
        }
        protoViewModel.fetchProtoList(null)
    }
}