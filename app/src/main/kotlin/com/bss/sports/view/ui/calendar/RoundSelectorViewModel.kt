package com.bss.sports.view.ui.calendar

import androidx.lifecycle.*
import com.bss.sports.repository.proto.ProtoRepository
import kotlinx.coroutines.launch

class RoundSelectorViewModel(private var protoRepository: ProtoRepository) : ViewModel() {
    var isVisible: MutableLiveData<Boolean> = MutableLiveData(false)
    private val protoListResponse = protoRepository.protoListResponse

    val currentRoundInfo: LiveData<String> = this.protoListResponse.map { response ->
        "${response.currentRound?.year ?: ""}년 ${response.currentRound?.outerRound ?: ""}회차"
    }

    private fun fetchProtoList(roundId: Long?) {
        viewModelScope.launch {
            protoRepository.loadProtoResponse(roundId)
        }
    }

    fun moveToNextRound() {
        fetchProtoList(protoListResponse.value?.nextRound?.id)
    }

    fun moveToPreviousRound() {
        fetchProtoList(protoListResponse.value?.previousRound?.id)
    }
}