package com.bss.sports.view.viewholder

import android.view.View
import com.bss.sports.base.BindingViewHolder
import com.bss.sports.databinding.ItemSportsBinding
import com.bss.sports.model.sports.BaseSportsModel

class SportsViewHolder(view: View) : BindingViewHolder<ItemSportsBinding>(view) {
    fun bindItem(sportsItem: BaseSportsModel) {
        binding.run {
            item = sportsItem
            executePendingBindings()
        }
    }
}