package com.bss.sports.view.ui.main.sports

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.bss.sports.R
import com.bss.sports.base.BindingFragment
import com.bss.sports.databinding.FragmentSportsBinding
import com.bss.sports.view.adapter.SportsAdapter
import com.bss.sports.view.ui.main.MainActivity
import com.bss.sports.view.ui.main.MainFragmentStateAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class SportsFragment : BindingFragment<FragmentSportsBinding>() {
    companion object {
        const val ARG_MainTab = "ARG_MainTab"

        fun createFragment(mainTab: MainFragmentStateAdapter.MainTab): SportsFragment {
            val fragment = SportsFragment()
            fragment.arguments = Bundle().apply {
                putSerializable(ARG_MainTab, mainTab)
            }
            return fragment
        }
    }

    private lateinit var tab: MainFragmentStateAdapter.MainTab
    private val sportsViewModel: SportsViewModel by viewModel { parametersOf(tab) }

    override fun getLayoutResId(): Int = R.layout.fragment_sports

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(ARG_MainTab) }?.apply {
            tab = getSerializable(ARG_MainTab) as MainFragmentStateAdapter.MainTab
            Timber.e(tab.name)
        }
        binding.apply {
            vm = sportsViewModel
            lifecycleOwner = this@SportsFragment
            sportsAdapter = SportsAdapter()
        }
    }

    private val observer = Observer<String> {
        Timber.e("observer ${tab.name} $it")
        sportsViewModel.fetchSportsList(it)
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity)?.calendarViewModel?.currentDate?.observe(
            viewLifecycleOwner,
            observer
        )
    }

    override fun onPause() {
        super.onPause()
        (activity as? MainActivity)?.calendarViewModel?.currentDate?.removeObserver(observer)
    }
}
