package com.bss.sports.view.ui.main

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.bss.sports.R
import com.bss.sports.base.BindingActivity
import com.bss.sports.databinding.ActivityMainBinding
import com.bss.sports.view.ui.calendar.CalendarViewModel
import com.bss.sports.view.ui.calendar.RoundSelectorViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BindingActivity<ActivityMainBinding>() {
    val calendarViewModel: CalendarViewModel by viewModel()
    val roundSelectorViewModel: RoundSelectorViewModel by viewModel()

    override fun getLayoutResId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.apply {
            cvm = calendarViewModel
            rvm = roundSelectorViewModel
            pagerAdapter = MainFragmentStateAdapter(this@MainActivity)
            lifecycleOwner = this@MainActivity
        }

        binding.pager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (MainFragmentStateAdapter.MainTab.프로토.ordinal == position) {
                    calendarViewModel.isVisible.postValue(false)
                    roundSelectorViewModel.isVisible.postValue(true)
                } else {
                    calendarViewModel.isVisible.postValue(true)
                    roundSelectorViewModel.isVisible.postValue(false)
                }
            }
        })
    }
}
