package com.bss.sports.view.ui.main.sports

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.bss.sports.model.sports.BaseSportsModel
import com.bss.sports.repository.sports.SportsRepository
import kotlinx.coroutines.Dispatchers

class SportsViewModel constructor(
    private val sportsRepository: SportsRepository
) : ViewModel() {
    private var sportsListFetchingLiveData: MutableLiveData<String> = MutableLiveData()
    val sportsList: LiveData<List<BaseSportsModel>>

    init {
        sportsList = this.sportsListFetchingLiveData.switchMap {
            liveData(Dispatchers.IO) {
                emitSource(sportsRepository.loadSportsData(it))
            }
        }
    }

    fun fetchSportsList(date: String) = this.sportsListFetchingLiveData.postValue(date)
}