package com.bss.sports.view.ui.main.proto

import androidx.lifecycle.*
import com.bss.sports.model.proto.RoundGame
import com.bss.sports.repository.proto.ProtoRepository
import kotlinx.coroutines.launch

class ProtoViewModel(private val protoRepository: ProtoRepository) : ViewModel() {
    val protoList: LiveData<List<RoundGame>> = this.protoRepository.protoListResponse.switchMap {
        MutableLiveData<List<RoundGame>>().apply {
            postValue(it.roundGames)
        }
    }

    fun fetchProtoList(roundId: Long?) {
        viewModelScope.launch {
            protoRepository.loadProtoResponse(roundId)
        }
    }
}