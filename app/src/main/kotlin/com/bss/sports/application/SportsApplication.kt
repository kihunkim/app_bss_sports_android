package com.bss.sports.application

import android.app.Application
import com.bss.sports.BuildConfig
import com.bss.sports.di.networkModule
import com.bss.sports.di.prefModule
import com.bss.sports.di.repositoryModule
import com.bss.sports.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class SportsApplication : Application(){
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@SportsApplication)
            modules(
                networkModule,
                prefModule,
                repositoryModule,
                viewModelModule
            )
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}