package com.bss.sports.application

object SportsConst {
    enum class LocationType {
        HOME, AWAY
    }

    enum class GameStatus {
        READY,
        IN_PROGRESS,
        FINAL,
        BREAK_TIME,  // 하프타임
        PAUSE,  // 잠시중단
        PENDING, // 중단
        POSTPONED, // 연기
        CUT, // 중단
        CANCEL
    }

    enum class SportsType {
        SOCCER,
        BASEBALL,
        BASKETBALL,
        VOLLEYBALL
    }

    enum class SportsResult {
        WIN, LOSE, DRAW
    }
}