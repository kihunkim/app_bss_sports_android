package com.bss.sports.model.sports

import com.bss.sports.application.SportsConst
import com.bss.sports.extensions.getTeamScore
import java.util.HashSet

data class Soccer(
    var id: Long = 0,
    var sportsType: String? = null,
    var realStartDateTime: String? = null,
    var venueName: String? = null,
    var period: Int = 0,
    var displayTime: String? = null,
    var videoLink: String? = null,
    var hasLineup: Boolean = false,
    var oddsFlag: Boolean = false,
    var teams: Teams? = null,
    var broadcasts: MutableList<Broadcast?>? = null,
    var headToHeadPerformance: String = ""
) : BaseSportsModel() {

    init {
        broadcast = broadcasts?.first()
    }

    override fun getTeamName(locationType: SportsConst.LocationType): String {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.name ?: ""
            SportsConst.LocationType.AWAY -> teams?.away?.name ?: ""
        }
    }

    override fun getTeamScore(locationType: SportsConst.LocationType): Int {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.periodData?.getTeamScore() ?: 0
            SportsConst.LocationType.AWAY -> teams?.away?.periodData?.getTeamScore() ?: 0
        }
    }
}

data class Teams(
    val away: Team? = null,
    val home: Team? = null
)