package com.bss.sports.model.sports

import com.bss.sports.application.SportsConst
import com.bss.sports.extensions.getTeamScore

data class Basketball(
    var id: Long = 0,
    var sportsType: String? = null,
    var realStartDateTime: String? = null,
    var period: Int = 0,
    var displayTime: String? = null,
    var hasLineup: Boolean = false,
    var oddsFlag: Boolean = false,
    var teams: BasketballTeams? = null,
    var special: Special? = null,
    var periodSpecial: List<PeriodSpecial>? = null
) : BaseSportsModel() {
    override fun getTeamName(locationType: SportsConst.LocationType): String {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.name ?: ""
            SportsConst.LocationType.AWAY -> teams?.away?.name ?: ""
        }
    }

    override fun getTeamScore(locationType: SportsConst.LocationType): Int {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.periodData?.getTeamScore() ?: 0
            SportsConst.LocationType.AWAY -> teams?.away?.periodData?.getTeamScore() ?: 0
        }
    }

    data class Special(
        val firstPoint: String? = null,
        val firstFive: String? = null,
        val firstSeven: String? = null,
        val firstTen: String? = null
    )

    data class PeriodSpecial(
        val period: Int = 0,
        val firstThreePoint: String? = null,
        val firstFreeThrow: String? = null
    )
}

data class BasketballTeams(
    val away: BasketballTeam? = null,
    val home: BasketballTeam? = null
)

data class BasketballTeam(
    var seasonRecord: BasketballSeasonRecord? = null
) : Team()

data class BasketballSeasonRecord(
    val ranking: Int = 0,
    val gameCount: Int = 0,
    val winCount: Int = 0,
    val defeatCount: Int = 0,
    val drawCount: Int = 0,
    val winPercentage: String? = null,
    val averageScore: String? = null,
    val fieldGoalSuccessRate: String? = null,
    val threePointShotSuccessRate: String? = null,
    val freeThrowSuccessRate: String? = null
)