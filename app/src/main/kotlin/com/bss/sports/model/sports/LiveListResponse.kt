package com.bss.sports.model.sports

data class LiveListResponse(
    val soccer: List<Soccer>,
    val baseball: List<Baseball>,
    val basketball: List<Basketball>,
    val volleyball: List<Volleyball>,
    val hockey: List<Hockey>,
    val football: List<Football>
)