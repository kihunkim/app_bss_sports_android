package com.bss.sports.model.sports

import com.bss.sports.application.SportsConst
import com.bss.sports.extensions.getTeamScore

data class Football(
    var id: Long = 0,
    var realStartDateTime: String? = null,
    var venueName: String? = null,
    var sportsType: String? = null,
    var period: Int = 0,
    var displayTime: String? = null,
    var videoLink: String? = null,
    var hasLineup: Boolean = false,
    var oddsFlag: Boolean = false,
    var teams: FootballTeams? = null,
    var special: FootballSpecial? = null
) : BaseSportsModel() {
    override fun getTeamName(locationType: SportsConst.LocationType): String {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.name ?: ""
            SportsConst.LocationType.AWAY -> teams?.away?.name ?: ""
        }
    }

    override fun getTeamScore(locationType: SportsConst.LocationType): Int {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.periodData?.getTeamScore() ?: 0
            SportsConst.LocationType.AWAY -> teams?.away?.periodData?.getTeamScore() ?: 0
        }
    }
}

data class FootballSpecial(
    val firstPoint: String? = null
)

data class FootballTeams(
    val away: FootballTeam? = null,
    val home: FootballTeam? = null
) {
    data class FootballTeam(
        var seasonRecord: FootballSeasonRecord? = null
    ) : Team()
}

data class FootballSeasonRecord(
    val ranking: Int = 0,
    val point: Int = 0,
    val gameCount: Int = 0,
    val winCount: Int = 0,
    val defeatCount: Int = 0,
    val setEarnRate: Int = 0,
    val scoreEarnRate: Int = 0
)