package com.bss.sports.model.proto

import com.bss.sports.application.SportsConst
import com.bss.sports.extensions.getTeamScore
import com.bss.sports.model.sports.*
import java.util.*

data class ProtoListResponse(
    val currentRound: ProtoRound? = null,
    val nextRound: ProtoRound? = null,
    val previousRound: ProtoRound? = null,
    val roundGames: List<RoundGame>
)

data class RoundGame(
    var soccerGame: Soccer? = null,
    var baseballGame: Baseball? = null,
    var basketballGame: Basketball? = null,
    var volleyballGame: Volleyball? = null,
    var displayName: String? = null,
    var groupType: String? = null,
    var id: Long = 0,
    var number: String = "0",
    var oddss: ArrayList<Odds>? = null,
    var pickCount: Int = 0,
    var pickCounts: List<PickCount>? = null,
    var saleStatus: String? = null,
    var sportsId: Int = 0,
    var sportsType: SportsConst.SportsType? = null,
    var startAt: String? = null,
    var type: String? = null
) : BaseSportsModel() {
    override fun getTeamName(locationType: SportsConst.LocationType): String {
        return when (locationType) {
            SportsConst.LocationType.HOME -> getTeam(SportsConst.LocationType.HOME)?.name ?: ""
            SportsConst.LocationType.AWAY -> getTeam(SportsConst.LocationType.AWAY)?.name ?: ""
        }
    }

    override fun getTeamScore(locationType: SportsConst.LocationType): Int {
        return when (locationType) {
            SportsConst.LocationType.HOME -> getTeam(SportsConst.LocationType.HOME)?.periodData?.getTeamScore()
                ?: 0
            SportsConst.LocationType.AWAY -> getTeam(SportsConst.LocationType.AWAY)?.periodData?.getTeamScore()
                ?: 0
        }
    }

    private fun getTeam(locationType: SportsConst.LocationType): Team? {
        if (locationType == SportsConst.LocationType.HOME) {
            return when (sportsType) {
                SportsConst.SportsType.SOCCER -> soccerGame?.teams?.home
                SportsConst.SportsType.BASEBALL -> baseballGame?.teams?.home
                SportsConst.SportsType.BASKETBALL -> basketballGame?.teams?.home
                SportsConst.SportsType.VOLLEYBALL -> volleyballGame?.teams?.home
                else -> null
            }
        } else {
            return when (sportsType) {
                SportsConst.SportsType.SOCCER -> soccerGame?.teams?.away
                SportsConst.SportsType.BASEBALL -> baseballGame?.teams?.away
                SportsConst.SportsType.BASKETBALL -> basketballGame?.teams?.away
                SportsConst.SportsType.VOLLEYBALL -> volleyballGame?.teams?.away
                else -> null
            }
        }
    }
}

data class ProtoRound(
    val displayName: String? = "",
    val endAt: String? = "",
    val id: Long = 0,
    val outerRound: Int = 0,
    val startAt: String? = "",
    val status: String? = "",
    val type: String? = "",
    val year: String? = ""
)

data class Odds(
    val availableFlag: Boolean = false,
    val choiceValue: String? = null,
    val id: Int = 0,
    val latestFlag: Boolean = false,
    val odds: Double? = null,
    val optionValue: Double? = null,
    val preOdds: Double? = null,
    val preOptionValue: Double? = null,
    val result: String? = "",
    val sequence: Int = 0,
    val slot: Int = 0,
    val type: String? = "",
    val createAt: String? = "",
    val updateType: String? = ""
)

data class PickCount(
    val count: Int = 0,
    val oddsType: String? = "",
    val slot: Int = 0
)

enum class UpdateType {
    ALL, ODDS, OPTION
}