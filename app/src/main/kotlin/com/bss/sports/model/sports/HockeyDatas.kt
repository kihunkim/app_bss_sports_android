package com.bss.sports.model.sports

import com.bss.sports.application.SportsConst
import com.bss.sports.extensions.getTeamScore
import java.util.HashSet

data class Hockey(
    var id: Long = 0,
    var sportsType: String? = null,
    var realStartDateTime: String? = null,
    var venueName: String? = null,
    var period: Int = 0,
    var displayTime: String? = null,
    var videoLink: String? = null,
    var hasLineup: Boolean = false,
    var oddsFlag: Boolean = false,
    var teams: HockeyTeams? = null,
    var special: HockeySpecial? = null
) : BaseSportsModel() {
    override fun getTeamName(locationType: SportsConst.LocationType): String {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.name ?: ""
            SportsConst.LocationType.AWAY -> teams?.away?.name ?: ""
        }
    }

    override fun getTeamScore(locationType: SportsConst.LocationType): Int {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.periodData?.getTeamScore() ?: 0
            SportsConst.LocationType.AWAY -> teams?.away?.periodData?.getTeamScore() ?: 0
        }
    }
}

data class HockeySpecial(
    val firstPoint: String? = null
)

data class HockeyTeams(
    val away: HockeyTeam? = null,
    val home: HockeyTeam? = null
)

data class HockeyTeam(
    var seasonRecord: HockeySeasonRecord? = null
) : Team()

data class HockeySeasonRecord(
    val ranking: Int = 0,
    val point: Int = 0,
    val gameCount: Int = 0,
    val winCount: Int = 0,
    val extraWinCount: Int = 0,
    val extraDefeatCount: Int = 0,
    val totalGetScore: Int = 0,
    val totalLoseScore: Int = 0
)