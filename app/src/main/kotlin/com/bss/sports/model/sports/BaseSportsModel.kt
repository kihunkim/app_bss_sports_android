package com.bss.sports.model.sports

import android.graphics.Color
import androidx.annotation.ColorInt
import com.bss.sports.application.SportsConst

abstract class BaseSportsModel {
    var league: League? = null
    var startDatetime: String? = null
    var result: SportsConst.SportsResult? = null
    var broadcast: Broadcast? = null
    private var gameStatus: SportsConst.GameStatus? = null

    abstract fun getTeamName(locationType: SportsConst.LocationType): String

    fun getLeagueName(): String {
        return league?.shortName ?: ""
    }

    fun getStartTime(): String {
        return startDatetime ?: ""
    }

    fun getPlayText(): String {
        return broadcast?.playText ?: ""
    }

    fun getGameStatus(): String {
        return gameStatus?.name ?: ""
    }

    abstract fun getTeamScore(locationType: SportsConst.LocationType): Int

    @ColorInt
    fun getTeamScoreColor(locationType: SportsConst.LocationType): Int {
        val isHome = locationType == SportsConst.LocationType.HOME
        val standardScore: Int =
            if (isHome) getTeamScore(SportsConst.LocationType.HOME)
            else getTeamScore(SportsConst.LocationType.AWAY)
        val compareScore: Int =
            if (isHome) getTeamScore(SportsConst.LocationType.AWAY)
            else getTeamScore(SportsConst.LocationType.HOME)
        return when {
            standardScore > compareScore -> Color.parseColor("#ec2529")
            standardScore < compareScore -> Color.parseColor("#8c8c8c")
            else -> Color.parseColor("#292929")
        }
    }
}