package com.bss.sports.model.sports

import com.bss.sports.application.SportsConst
import com.bss.sports.extensions.getTeamScore

data class Baseball(
    var id: Long = 0,
    var sportsType: String? = null,
    var period: Int = 0,
    var displayTime: String? = null,
    var hasLineup: Boolean = false,
    var teams: BaseballTeams? = null,
    var inningDivision: String? = null,
    var strike: Int = 0,
    var out: Int = 0,
    var ball: Int = 0,
    var firstBaseOccupied: Boolean = false,
    var secondBaseOccupied: Boolean = false,
    var thirdBaseOccupied: Boolean = false,
    var currentPitcher: Player? = null,
    var currentBatter: Player? = null,
    var special: Special? = null
) : BaseSportsModel() {
    override fun getTeamName(locationType: SportsConst.LocationType): String {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.name ?: ""
            SportsConst.LocationType.AWAY -> teams?.away?.name ?: ""
        }
    }

    override fun getTeamScore(locationType: SportsConst.LocationType): Int {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.periodData?.getTeamScore() ?: 0
            SportsConst.LocationType.AWAY -> teams?.away?.periodData?.getTeamScore() ?: 0
        }
    }
}

data class Special(
    val firstBaseOnBall: SpecialData? = null,
    val firstHomerun: SpecialData? = null,
    val firstStrikeOut: SpecialData? = null
) {
    data class SpecialData(
        val location: String? = null,
        val period: Int = 0
    )
}

data class BaseballTeams(
    val away: BaseballTeam? = null,
    val home: BaseballTeam? = null
)

data class BaseballTeam(
    var baseOnBallCount: Int = 0,
    var errorCount: Int = 0,
    var hitCount: Int = 0,
    var startPitcher: Player? = null,
    var seasonRecord: BaseballSeasonRecord? = null
) : Team()

data class BaseballSeasonRecord(
    val ranking: Int = 0,
    val gameCount: Int = 0,
    val winCount: Int = 0,
    val defeatCount: Int = 0,
    val drawCount: Int = 0,
    val winPercentage: String? = null,
    val streak: String? = null,
    val performance: String? = null
)