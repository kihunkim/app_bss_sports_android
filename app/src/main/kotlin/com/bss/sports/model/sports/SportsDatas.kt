package com.bss.sports.model.sports

import com.bss.sports.application.SportsConst

data class League(
    var id: Int = 0,
    var name: String? = "",
    var shortName: String? = "",
    var color: String? = ""
)

open class Team(
    var id: Int = 0,
    var name: String? = "",
    var shortName: String? = "",
    var imgPath: String? = "",
    var periodData: List<PeriodData>? = null,
    var seasonTeamRanking: Int = 0
)

data class PeriodData(
    val period: Int = 0,
    val score: Int = 0,
    val foul: Int = 0
)

data class Player(
    val id: Int = 0,
    val name: String? = null,
    val shortName: String? = null
)

data class Broadcast(
    var score: Score? = null,
    var playText: String? = "",
    var locationType: SportsConst.LocationType? = null,
    var eventType: String? = "",
    var displayTime: String? = ""
) {
    data class Score(
        val home: Int = 0,
        val away: Int = 0
    )
}
