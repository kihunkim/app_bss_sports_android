package com.bss.sports.model.sports

import com.bss.sports.application.SportsConst
import com.bss.sports.extensions.getTeamScore

data class Volleyball(
    var id: Long = 0,
    var sportsType: String? = null,
    var realStartDateTime: String? = null,
    var venueName: String? = null,
    var period: Int = 0,
    var displayTime: String? = null,
    var videoLink: String? = null,
    var hasLineup: Boolean = false,
    var teams: VolleyballTeams? = null,
    var special: VolleyballSpecial? = null,
    var oddsFlag: Boolean = false
) : BaseSportsModel() {
    override fun getTeamName(locationType: SportsConst.LocationType): String {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.name ?: ""
            SportsConst.LocationType.AWAY -> teams?.away?.name ?: ""
        }
    }

    override fun getTeamScore(locationType: SportsConst.LocationType): Int {
        return when (locationType) {
            SportsConst.LocationType.HOME -> teams?.home?.periodData?.getTeamScore() ?: 0
            SportsConst.LocationType.AWAY -> teams?.away?.periodData?.getTeamScore() ?: 0
        }
    }
}

data class VolleyballSpecial(
    val firstFive: String? = null,
    val firstSeven: String? = null,
    val firstTen: String? = null
)

data class VolleyballTeams(
    val away: VolleyballTeam? = null,
    val home: VolleyballTeam? = null
)

data class VolleyballTeam(
    var seasonRecord: VolleyballSeasonRecord? = null
) : Team()

data class VolleyballSeasonRecord(
    val ranking: Int = 0,
    val point: Int = 0,
    val gameCount: Int = 0,
    val winCount: Int = 0,
    val defeatCount: Int = 0,
    val setEarnRate: String? = null,
    val scoreEarnRate: String? = null
)