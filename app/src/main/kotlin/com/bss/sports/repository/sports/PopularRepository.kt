package com.bss.sports.repository.sports

import androidx.lifecycle.MutableLiveData
import com.bss.sports.model.sports.BaseSportsModel
import com.bss.sports.network.SportsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PopularRepository(private val sportsApi: SportsApi) : SportsRepository {
    override suspend fun loadSportsData(date: String) =
        withContext(Dispatchers.IO) {
            MutableLiveData<List<BaseSportsModel>>().apply {
                val response = sportsApi.getPopularGames(date)
                postValue(mutableListOf<BaseSportsModel>().apply {
                    addAll(response.soccer)
                    addAll(response.baseball)
                    addAll(response.basketball)
                    addAll(response.volleyball)
                    addAll(response.hockey)
                    addAll(response.football)
                })
            }
        }
}