package com.bss.sports.repository.sports

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bss.sports.model.sports.BaseSportsModel

interface SportsRepository {
    suspend fun loadSportsData(date: String) : LiveData<List<BaseSportsModel>>
}