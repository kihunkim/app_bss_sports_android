package com.bss.sports.repository.proto

import androidx.lifecycle.MutableLiveData
import com.bss.sports.model.proto.ProtoListResponse
import com.bss.sports.network.ProtoApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ProtoRepository(private val protoApi: ProtoApi) {
    var protoListResponse = MutableLiveData<ProtoListResponse>()

    suspend fun loadProtoResponse(roundId: Long?) {
        withContext(Dispatchers.IO) {
            protoListResponse.postValue(protoApi.getProtoWinList(roundId))
        }
    }
}