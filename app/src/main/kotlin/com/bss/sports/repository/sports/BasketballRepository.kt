package com.bss.sports.repository.sports

import androidx.lifecycle.MutableLiveData
import com.bss.sports.model.sports.BaseSportsModel
import com.bss.sports.network.SportsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BasketballRepository(private val sportsApi: SportsApi) : SportsRepository {
    override suspend fun loadSportsData(date: String) =
        withContext(Dispatchers.IO) {
            MutableLiveData<List<BaseSportsModel>>().apply {
                postValue(sportsApi.getBasketballList(date))
            }
        }
}